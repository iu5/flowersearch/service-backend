import numpy as np

from src.index import Index


class SearchEngine:
    def __init__(self, imgs_path, embs_path, method='hnsw', space='cosinesimil'):
        self.paths = np.load(imgs_path)
        self.index = Index(np.load(embs_path))

    def rank(self, embedding, top_k):
        ids, distances = self.index.rank(embedding, top_k)
        return self.paths[ids]
