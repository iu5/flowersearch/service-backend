from io import BytesIO
from typing import List

import yaml
import numpy as np
from PIL import Image
from fastapi import FastAPI, File, UploadFile
from pydantic import BaseModel
from fastapi.staticfiles import StaticFiles

from src.triton import parse_triton
from src.engine import SearchEngine

app = FastAPI(title='Flower Search')
app.mount("/mnt/pmldl/flowers104", StaticFiles(directory="/images"), name="images")

app.config = yaml.load(open("/docker/config.yaml", "r"), Loader=yaml.FullLoader)
app.triton_client = parse_triton(app.config["triton"]['efficient_embedder'])
app.search_engine = SearchEngine(**app.config["engine"])


def read_image(file) -> Image.Image:
    image = np.array(Image.open(BytesIO(file)))
    return image


@app.get("/ping")
def ping():
    return "Pong"


class RankedPaths(BaseModel):
    top_paths: List[str]


@app.post("/rank", response_model=RankedPaths)
async def predict_api(file: UploadFile = File(...)):
    extension = file.filename.split(".")[-1] in ("jpg", "jpeg", "png")
    if not extension:
        return "Image must be jpg or png format!"

    image = read_image(await file.read())
    prediction = app.triton_client.execute([image])["output__0"][0]
    top_paths = app.search_engine.rank(prediction, top_k=100).tolist()

    return RankedPaths(top_paths=top_paths)
