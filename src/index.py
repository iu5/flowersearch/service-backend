import nmslib
import numpy as np


class Index:
    def __init__(self, embeddings, method='hnsw', space='cosinesimil'):
        self.index = nmslib.init(method='hnsw', space='cosinesimil')
        self.index.addDataPointBatch(embeddings)
        self.index.createIndex({'post': 2}, print_progress=True)

    def rank(self, embedding, top_k):
        ids, distances = self.index.knnQuery(embedding, k=top_k)
        return ids, distances
