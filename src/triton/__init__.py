from typing import Union, Dict

import yaml

from .client import TritonImageClient
from .structure import TritonImageClientStructure
from . import transforms
from .transforms import Transform

TRANSFORMS: dict = {
    k: v
    for k, v in transforms.__dict__.items()
    if isinstance(v, type) and issubclass(v, Transform)
}


def parse_triton(config: Union[Dict, str]):
    if isinstance(config, str):
        config: dict = yaml.load(open(config, "r"), Loader=yaml.FullLoader)
    params = TritonImageClient.deserialize(config, TRANSFORMS)
    wrapper = TritonImageClient(**params)
    return wrapper
