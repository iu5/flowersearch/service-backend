from typing import List

import cv2
import nmslib
import numpy as np
import torch
from torch.utils.data import Dataset, DataLoader


class Transform:
    def __init__(self, **params):
        self.params = params

    def __call__(self, *args, **kwargs):
        return self.apply(*args, **kwargs)

    def apply(self, *args, **kwargs):
        return NotImplementedError


class ImageTransform(Transform):
    def __call__(self, image: np.ndarray) -> np.ndarray:
        return self.apply(image)

    def apply(self, image: np.ndarray) -> np.ndarray:
        raise NotImplementedError

    def _check(self, fields: List[str]):
        for field in fields:
            assert field in self.params, f"{field} is required field"


class Compose(Transform):
    def __init__(self, transforms: List[Transform]):
        for transform in transforms:
            assert isinstance(transform, Transform), f"{type(transform)} is not subclass of {Transform}"
        super().__init__(transforms=transforms)

    def apply(self, item: np.ndarray):
        if isinstance(item, np.ndarray):
            output = item.copy()
        else:
            raise TypeError(f"Type {type(item)} is not allowed")
        for transform in self.params["transforms"]:
            output = transform(output)

        return output

    def __str__(self):
        return "Compose  \n  " + "\n  ".join([str(transform)
                                              for transform in self.params["transforms"]])


class ToTensorWithNormalization(ImageTransform):
    def __init__(self, mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225), max_pixel_value=255.0):
        super().__init__()
        self.mean = mean
        self.std = std
        self.max_pixel_value = max_pixel_value

    @staticmethod
    def normalize_func(tensor, mean, std, inplace=False):
        if not inplace:
            tensor = tensor.clone()

        dtype = tensor.dtype
        mean = torch.as_tensor(mean, dtype=dtype, device=tensor.device)
        std = torch.as_tensor(std, dtype=dtype, device=tensor.device)
        tensor.sub_(mean[:, None, None]).div_(std[:, None, None])

        return tensor

    def apply(self, image: np.ndarray):
        tensor = torch.from_numpy(
            np.moveaxis(image / (255.0 if image.dtype == np.uint8 else 1), -1, 0).astype(np.float32))
        return self.normalize_func(tensor, mean=self.mean, std=self.std)


class Resize(ImageTransform):
    def apply(self, image: np.ndarray):
        self._check(["height", "width"])
        return cv2.resize(image, (self.params["width"], self.params["height"]))
