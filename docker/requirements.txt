python-multipart
pyyaml>=5.3
numpy>=1.19
pillow>=8.0
opencv-python
nmslib
torch